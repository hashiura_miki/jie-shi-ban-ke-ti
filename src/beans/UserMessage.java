package beans;

import java.io.Serializable;
import java.sql.Timestamp;

public class UserMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String name;
    private int user_id;
    private String title;
    private String text;
    private String category;
    private Timestamp created_date;

    public int getId(){
		return this.id;
	}
		public String getName(){
		return this.name;
	}
	public int getUser_id(){
		return this.user_id;
	}
	public String getTitle(){
		return this.title;
	}
	public String getText(){
		return this.text;
	}
	public String getCategory(){
		return this.category;
	}
	public Timestamp getCreated_date(){
		return this.created_date;
	}


	public void setId(int id) {
		this.id = id;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setUser_id(int user_id){
		this.user_id = user_id;
	}
	public void setTitle(String title){
		this.title = title;
	}
	public void setText(String text){
		this.text = text;
	}
	public void setCategory(String category){
		this.category = category;
	}
	public void setCreated_date(Timestamp created_date){
		this.created_date = created_date;
	}

}
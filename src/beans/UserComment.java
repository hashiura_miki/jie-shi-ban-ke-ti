package beans;

import java.io.Serializable;
import java.sql.Timestamp;

public class UserComment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String name;
    private int user_id;
    private int message_id;
    private String text;
    private Timestamp created_date;

    public int getId(){
		return this.id;
	}
		public String getName(){
		return this.name;
	}
	public int getUser_id(){
		return this.user_id;
	}
	public int getMessage_id(){
		return this.message_id;
	}
	public String getText(){
		return this.text;
	}
	public Timestamp getCreated_date(){
		return this.created_date;
	}


	public void setId(int id) {
		this.id = id;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setUser_id(int user_id){
		this.user_id = user_id;
	}
	public void setMessage_id(int message_id){
		this.message_id = message_id;
	}
	public void setText(String text){
		this.text = text;
	}
	public void setCreated_date(Timestamp created_date){
		this.created_date = created_date;
	}

}
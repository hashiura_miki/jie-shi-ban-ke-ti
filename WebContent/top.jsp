<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/topStyle.css" rel="stylesheet" type="text/css">
</head>
<title>Home</title>
</head>
<body>
	<div class="header">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<c:if test="${ empty loginUser }">
			<a href="login">ログイン</a>
		</c:if>
		<c:if test="${ not empty loginUser }">
			<a href="logout" style="position: absolute; right: 100px;">ログアウト</a>
			<div class="profile">
				<div class="name">
					<h2>
						@
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
			</div>
			<a href="./newMessage" style="font-size: 140%">◎新規投稿</a>
			<c:if test="${loginUser.position_id == 1}">
				<div class="position_id">
					<a href="./UserList" style="font-size: 140%">◎ユーザー管理</a>
				</div>
			</c:if>
		</c:if>
	</div>


	<c:if test="${ not empty loginUser }">

		<div class="search">
			<form action="./" method="get">
				<p class="txt">
					<br /> <input type="date" name="start_time" value="${start_time}"></input>から
					<input type="date" name="finish_time" value="${finish_time}"></input>の期間で<br />カテゴリー
					<input type="text" name="check_text" value="${check_text}">
					を含む投稿
					<button type="submit" name="start_time finish_time check_text">検索</button>
				</p>
			</form>
			<br />
		</div>

		<c:forEach items="${messages}" var="message">
			<div class="messages-comments">
				<br /> <br />
				<div class="message-title">
					『
					<c:out value="${message.title}" />
					』
				</div>

				<div class="message-text">
					<pre style="font-size: 21px;"><c:out value="${message.text}" /></pre>
				</div>

				<div class="message-category">
					カテゴリー＜
					<c:out value="${message.category}" />
					＞
				</div>

				<div class="message-name">
					投稿 by.
					<c:out value="${message.name}" />
				</div>

				<div class="message-date">
					<fmt:formatDate value="${message.created_date}"
						pattern="yyyy/MM/dd HH:mm:ss" />
				</div>

				<c:if test="${message.user_id == loginUser.id}">
					<div class="message_id">
						<form method=Post action="./MessageDelete">
							<button onClick="return confirm('投稿文を削除してもよろしいですか？')"
								type="submit" style="position: absolute; right: 200px;"
								name=message_id value="${message.id}">投稿文削除</button>
								<br />
						</form>
					</div>
				</c:if>
				<c:forEach items="${comments}" var="comment">
					<c:if test="${message.id == comment.message_id}">
						<div class="comment">
							<div class="comment-text">
								<pre><c:out value="${comment.text}" /></pre>
							</div>
							<div class="comment-name">
								コメント by.
								<c:out value="${comment.name}" />
							</div>
							<div class="comment-date">
								<fmt:formatDate value="${comment.created_date}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</div>
							<c:if test="${comment.user_id == loginUser.id}">
							<form method=Post action="./CommentDelete">
								<button onClick="return confirm('コメントを削除してもよろしいですか？')"
									type="submit" style="position: absolute; right: 250px;"
									name="comment_id" value="${comment.id}">コメント削除</button>
								<br />
							</form>
						</c:if>
						</div>
					</c:if>
					<br />
				</c:forEach>
				<div class="form-area">
					<form action="newComment" method="post">
						<textarea placeholder="コメントを入力" required maxlength='500'
							name="text" cols="100" rows="3"></textarea>
						<br /> <input type="hidden" name="message_id"
							value="${message.id}"> <input type="submit"
							style="position: absolute; right: 200px;" value="コメント投稿">
						<br /> <br />
					</form>
				</div>
			</div>
		</c:forEach>

	</c:if>
</body>
<br />
<div class="copyright">Copyright(c)HashiuraMiki</div>
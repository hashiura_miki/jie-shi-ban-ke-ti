<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/postStyle.css" rel="stylesheet" type="text/css">
<title>新規投稿</title>
</head>
<body>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
	<div class="form-area">
		<form action="newMessage" method="Post">
			<p class="txt">
				件名（1～30文字)<br /> <input type="text" size="60" required maxlength='30'name="title"><br />

				本文（1～1000文字)<br />
				<textarea required maxlength='1000' name="text" cols="100" rows="5"
					></textarea>
				<br />

				カテゴリー（1～10文字)<br /> <input type="text" size="20" required maxlength='10'name="category"> <br />
			</p>
			<br /> <input type="submit" value="投稿"><br />
		</form>

	</div>
	<br />
	<br />
	<a href="./">ホーム画面に戻る</a>
	<div class="copyright">Copyright(c)MikiHashiura</div>
	</div>
</html>
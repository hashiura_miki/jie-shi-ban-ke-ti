package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = {"/UserList","/UserEdit","/signup"})
public class AuthorityFilter implements Filter{
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{

		HttpSession session = ((HttpServletRequest)req).getSession();
		User user = (User) session.getAttribute("loginUser");

		if(user == null){
			// userがNullならば、ログイン画面へ飛ばす
			List<String> e_messages = new ArrayList<String>();
			e_messages.add("ログインしてください");
			session.setAttribute("errorMessages", e_messages);

			((HttpServletResponse)res).sendRedirect("./login");

		}

		else if(user.getPosition_id() == 1){
			// 総務人事なら通常どおりの遷移
			chain.doFilter(req, res);
		}else{
			// 総務人事以外ならtopへ飛ばす
			List<String> e_messages = new ArrayList<String>();
			e_messages.add("アクセス権限がありません");
			session.setAttribute("errorMessages", e_messages);
			//req.setAttribute("errorMessages", e_messages);

			((HttpServletResponse)res).sendRedirect("./");

		}
	}

	public void init(FilterConfig config) throws ServletException{}
	public void destroy(){}
}
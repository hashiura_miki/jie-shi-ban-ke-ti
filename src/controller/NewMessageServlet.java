package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		 request.getRequestDispatcher("post.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if (isValid(request, messages) == true) {
		User user = (User) session.getAttribute("loginUser");
		Message message = new Message();
		message.setTitle(request.getParameter("title"));
		message.setText(request.getParameter("text"));
		message.setCategory(request.getParameter("category"));
		message.setUserId(user.getId());

		new MessageService().register(message);

		response.sendRedirect("./");
		} else {
			//session.setAttribute("errorMessages", messages);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("post.jsp").forward(request, response);
		}
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {
//		String title = request.getParameter("title");
		String text = request.getParameter("text");
//		String category = request.getParameter("category");

//		if (StringUtils.isBlank(title)) {
//			messages.add("件名が未入力です");
//		}
//		if (StringUtils.isBlank(text)) {
//			messages.add("本文が未入力です");
//		}
		if (1000 < text.length()) {
			messages.add("本文は1000文字以下で入力して下さい");
		}
//		if (StringUtils.isBlank(category)) {
//			messages.add("カテゴリーが未入力です");
//		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
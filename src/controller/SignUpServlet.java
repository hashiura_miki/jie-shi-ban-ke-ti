package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		session.setAttribute("is_work", "1");
		request.getRequestDispatcher("signup.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if (isValid(request, messages) == true) {
			User user = new User();
			user.setUser_id(request.getParameter("user_id"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
			user.setPosition_id(Integer.parseInt(request.getParameter("position_id")));
			user.setIs_work(Byte.parseByte(request.getParameter("is_work")));

			new UserService().register(user);

			response.sendRedirect("./UserList");

		} else {
			//session.setAttribute("errorMessages", messages);
			request.setAttribute("errorMessages", messages);
			session.setAttribute("user_id", request.getParameter("user_id"));
			session.setAttribute("name", request.getParameter("name"));
			session.setAttribute("branch_id", request.getParameter("branch_id"));
			session.setAttribute("position_id", request.getParameter("position_id"));
			session.setAttribute("is_work", request.getParameter("is_work"));
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		int branch_id = Integer.parseInt(request.getParameter("branch_id"));
		int position_id = Integer.parseInt(request.getParameter("position_id"));
		new UserService();
		String user_id = request.getParameter("user_id");

		if (!(password.equals(password2))) {
			messages.add("確認用パスワードが一致しません");
		}
		if(UserService.UserIdCheck(user_id) != null) {
			messages.add("ユーザーIDがすでに使われています");
		}
		if(branch_id == 1) {
			if(!(position_id == 1 || position_id == 2)) {
				messages.add("支店と役職の組み合わせが不正です");
			}
		}
		else {
			if(!(position_id == 3 || position_id == 4)) {
				messages.add("支店と役職の組み合わせが不正です");
			}
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
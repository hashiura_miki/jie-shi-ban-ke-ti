package dao;
import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("user_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", position_id");
			sql.append(", is_work");
			sql.append(") VALUES (");
			sql.append("?"); // user_id
			sql.append(", ?"); // passeword
			sql.append(", ?"); // name
			sql.append(", ?"); // branch_id
			sql.append(", ?"); // position_id
			sql.append(", ?"); // is_work
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getUser_id());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch_id());
			ps.setInt(5, user.getPosition_id());
			ps.setByte(6, user.getIs_work());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}



	public void select(Connection connection) {
		PreparedStatement ps = null;
		try {

			String sql = "select * from users";
			ps = connection.prepareStatement(sql);

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty()) {
				return null;

			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			}else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	public User getUser(Connection connection, String user_id,String password) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE user_id = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, user_id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty()) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String user_id = rs.getString("user_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branch_id = rs.getInt("branch_id");
				int position_id = rs.getInt("position_id");
				byte is_work = rs.getByte("is_work");

				User user = new User();
				user.setId(id);
				user.setUser_id(user_id);
				user.setPassword(password);
				user.setName(name);
				user.setBranch_id(branch_id);
				user.setPosition_id(position_id);
				user.setIs_work(is_work);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}



	public void edit(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("user_id = ?");
			if(!(StringUtils.isEmpty(user.getPassword())))
				sql.append(", password = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", position_id = ?");
			sql.append(", is_work = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");


			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, user.getUser_id());
			if(!(StringUtils.isEmpty(user.getPassword()))) {
				ps.setString(2, user.getPassword());
				ps.setString(3, user.getName());
				ps.setInt(4, user.getBranch_id());
				ps.setInt(5, user.getPosition_id());
				ps.setByte(6, user.getIs_work());
				ps.setInt(7, user.getId());
			}
			else {
				ps.setString(2, user.getName());
				ps.setInt(3, user.getBranch_id());
				ps.setInt(4, user.getPosition_id());
				ps.setByte(5, user.getIs_work());
				ps.setInt(6, user.getId());
			}
			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void is_work(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			Byte is_work = user.getIs_work();
			if(is_work == 0)is_work = 1;
			else is_work = 0;

			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET is_work = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");


			ps = connection.prepareStatement(sql.toString());
			ps.setByte(1, is_work);
			ps.setInt(2, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}


	public User UserIdCheck(Connection connection, String user_id) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE user_id = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, user_id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty()) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}




package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.LoginService;
import service.UserService;
/**
 * Servlet implementation class CommentDelete
 */
@WebServlet("/UserEdit")
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if(isNumber(request.getParameter("id"))){
			int sql_id = Integer.parseInt(request.getParameter("id"));
			User user = new UserService().getUser(sql_id);
			request.setAttribute("user", user);

			if(user != null) {
				session.setAttribute("user_id", user.getUser_id());
				session.setAttribute("name", user.getName());
				session.setAttribute("branch_id",user.getBranch_id());
				session.setAttribute("position_id", user.getPosition_id());
				session.setAttribute("is_work", user.getIs_work());
				request.getRequestDispatcher("edit.jsp").forward(request, response);
			}else {
				messages.add("ユーザーが存在しません");
				session.setAttribute("errorMessages", messages);
				//request.setAttribute("errorMessages", messages);
				response.sendRedirect("./UserList");
			}
		}
		else {
			messages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", messages);
			//request.setAttribute("errorMessages", messages);
			response.sendRedirect("./UserList");
		}
	}

	public boolean isNumber(String num) {
		try {
			Integer.parseInt(num);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if (isValid(request, messages)) {
			User editUser = new User();
			editUser.setId(Integer.parseInt(request.getParameter("id")));
			editUser.setUser_id(request.getParameter("user_id"));
			editUser.setPassword(request.getParameter("password"));
			editUser.setName(request.getParameter("name"));
			editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
			editUser.setPosition_id(Integer.parseInt(request.getParameter("position_id")));
			if(request.getParameter("is_work") == null)
				editUser.setIs_work(Byte.parseByte("1"));
			else
				editUser.setIs_work(Byte.parseByte(request.getParameter("is_work")));

			new UserService().edit(editUser);

			//ログインユーザーのユーザー情報を編集したときログイン情報も更新する
			if( request.getParameter("user.id") == request.getParameter("loginUser.id")){
				String user_id = request.getParameter("user_id");
				String password = request.getParameter("password");

				LoginService loginService = new LoginService();
				User user = loginService.login(user_id, password);
				session.setAttribute("loginUser", user);
			}

			response.sendRedirect("./UserList");

		} else {
			session.setAttribute("errorMessages", messages);
			session.setAttribute("user_id", request.getParameter("user_id"));
			session.setAttribute("name", request.getParameter("name"));
			session.setAttribute("branch_id", request.getParameter("branch_id"));
			session.setAttribute("position_id", request.getParameter("position_id"));
			session.setAttribute("is_work", request.getParameter("is_work"));

			int sql_id = Integer.parseInt(request.getParameter("id"));
			User user = new UserService().getUser(sql_id);
			request.setAttribute("user", user);

			request.getRequestDispatcher("edit.jsp").forward(request, response);
		}
	}



	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		int branch_id = Integer.parseInt(request.getParameter("branch_id"));
		int position_id = Integer.parseInt(request.getParameter("position_id"));
		new UserService();
		String user_id = request.getParameter("user_id");

		if (!(password.equals(password2))) {
			messages.add("確認用パスワードが一致しません");
		}

		if((UserService.UserIdCheck(user_id)) != null ){
			if(UserService.UserIdCheck(user_id).getId() != Integer.parseInt(request.getParameter("id"))){
				messages.add("ユーザーIDがすでに使われています");
			}
		}

		if(branch_id == 1) {
			if(!(position_id == 1 || position_id == 2)) {
				messages.add("支店と役職の組み合わせが不正です");
			}
		}
		else {
			if(!(position_id == 3 || position_id == 4)) {
				messages.add("支店と役職の組み合わせが不正です");
			}
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}



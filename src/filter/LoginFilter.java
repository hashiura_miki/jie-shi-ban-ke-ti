package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter{

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{

		HttpSession session = ((HttpServletRequest)req).getSession();
		String url = ((HttpServletRequest) req).getServletPath();

		if((!(url).equals("/css/loginStyle.css")) && (!(url).equals("/login"))) {
			User user = (User) session.getAttribute("loginUser");

			if(user != null){
				// userがNULLでなければ、通常どおりの遷移
				chain.doFilter(req, res);
			}else{
				// userがNullならば、ログイン画面へ飛ばす
				List<String> e_messages = new ArrayList<String>();
				e_messages.add("ログインしてください");
				session.setAttribute("errorMessages", e_messages);
				//req.setAttribute("errorMessages", e_messages);

				((HttpServletResponse)res).sendRedirect("./login");

			}

		}else{
			// urlが"/login"かつ".css"なら通常どおりの遷移
			chain.doFilter(req, res);
		}
	}

	public void init(FilterConfig config) throws ServletException{}
	public void destroy(){}


}
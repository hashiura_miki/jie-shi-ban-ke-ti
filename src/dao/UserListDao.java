package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserList;
import exception.SQLRuntimeException;

public class UserListDao {

	public List<UserList> getUserList(Connection connection) {

		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM  users");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserList> ret = toUserList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}



	private List<UserList> toUserList(ResultSet rs) throws SQLException {

		List<UserList> ret = new ArrayList<UserList>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String user_id = rs.getString("user_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branch_id = rs.getInt("branch_id");
				int position_id = rs.getInt("position_id");
				byte is_work = rs.getByte("is_work");

				UserList user = new UserList();
				user.setId(id);
				user.setUser_id(user_id);
				user.setPassword(password);
				user.setName(name);
				user.setBranch_id(branch_id);
				user.setPosition_id(position_id);
				user.setIs_work(is_work);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
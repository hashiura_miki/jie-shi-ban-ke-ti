package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserList;
import service.UserService;
/**
 * Servlet implementation class CommentDelete
 */
@WebServlet("/UserList")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		session.removeAttribute("user_id");
		session.removeAttribute("name");
		session.removeAttribute("branch_id");
		session.removeAttribute("position_id");
		session.removeAttribute("is_work");
		List<UserList> users = new UserService().getUser();

		request.setAttribute("users", users);

		request.getRequestDispatcher("/management.jsp").forward(request, response);
	}
}



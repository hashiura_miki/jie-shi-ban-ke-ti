package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("utf-8");

		String start_time = request.getParameter("start_time");
		if (StringUtils.isEmpty(start_time)) start_time = "2019-03-01";
		session.setAttribute("start_time", request.getParameter("start_time"));

		String finish_time = request.getParameter("finish_time");
		if (StringUtils.isEmpty(finish_time)) finish_time = "2019-04-30";
		session.setAttribute("finish_time", request.getParameter("finish_time"));

		String check_text = request.getParameter("check_text");
		if (StringUtils.isEmpty(check_text)) check_text = "";
		session.setAttribute("check_text", request.getParameter("check_text"));

		List<UserMessage> messages = new MessageService().getMessage(start_time,finish_time,check_text);
		request.setAttribute("messages", messages);

		List<UserComment> comments = new CommentService().getComment();
		request.setAttribute("comments", comments);

		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}



}
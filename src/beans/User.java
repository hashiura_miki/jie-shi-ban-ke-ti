package beans;

import java.io.Serializable;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String user_id;
	private String password;
	private String name;
	private int branch_id;
	private int position_id;
	private byte is_work;

	public int getId(){
		return this.id;
	}
	public String getUser_id(){
		return this.user_id;
	}
	public String getPassword(){
		return this.password;
	}
	public String getName(){
		return this.name;
	}
	public int getBranch_id(){
		return this.branch_id;
	}
	public int getPosition_id(){
		return this.position_id;
	}
	public byte getIs_work(){
		return this.is_work;
	}


	public void setId(int id) {
		this.id = id;
	}
	public void setUser_id(String user_id){
		this.user_id = user_id;
	}
	public void setPassword(String password){
		this.password = password;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setBranch_id(int branch_id){
		this.branch_id = branch_id;
	}
	public void setPosition_id(int position_id){
		this.position_id = position_id;
	}
	public void setIs_work(byte is_work){
		this.is_work = is_work;
	}
}
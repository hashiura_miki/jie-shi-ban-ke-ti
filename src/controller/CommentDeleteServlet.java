package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.CommentService;
/**
 * Servlet implementation class CommentDelete
 */
@WebServlet("/CommentDelete")
public class CommentDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		//HttpSession session = request.getSession();

		List<String> e_messages = new ArrayList<String>();
		e_messages.add("不正なアクセスです");
		//session.setAttribute("errorMessages", e_messages);
		request.setAttribute("errorMessages", e_messages);
		response.sendRedirect("login");

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		int comment_id = Integer.parseInt(request.getParameter("comment_id"));

		CommentService.delete(comment_id);

		response.sendRedirect("./");
	}
}




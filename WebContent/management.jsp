<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/managementStyle.css" rel="stylesheet" type="text/css">

<title>ユーザー管理</title>
</head>
<body>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
	<form method=Get action="./signup"></form>
	<div class="user">
		<br /> <a href="./signup" style="position: absolute; left: 350px;">新規ユーザー登録</a>
		<br /> <br />
		<table border="1" align="center">
			<tr>
				<th>ユーザーID</th>
				<th>名前</th>
				<th>支店</th>
				<th>部署・役職</th>
				<th>稼動状況</th>
				<th></th>
				<th>稼動切り替え</th>
			</tr>

			<c:forEach items="${users}" var="user">
				<tr>
					<td height="60">${user.user_id}</td>
					<td height="60">${user.name}</td>
					<td height="60"><c:choose>
							<c:when test="${user.branch_id == 1}">本社</c:when>
							<c:when test="${user.branch_id == 2}">支店A</c:when>
							<c:when test="${user.branch_id == 3}">支店B</c:when>
							<c:when test="${user.branch_id == 4}">支店C</c:when>
							<c:otherwise>その他</c:otherwise>
						</c:choose></td>
					<td height="60"><c:choose>
							<c:when test="${user.position_id == 1}">総務人事</c:when>
							<c:when test="${user.position_id == 2}">情報管理</c:when>
							<c:when test="${user.position_id == 3}">支店長</c:when>
							<c:otherwise>支店社員</c:otherwise>
						</c:choose></td>
					<c:if test="${user.is_work == 1}">
						<td height="60"><font size="3" color="blue">稼動中</font></td>
					</c:if>
					<c:if test="${user.is_work == 0}">
						<td height="60"><font size="3" color="red">停止中</font></td>
					</c:if>
					<td>
						<form action="./UserEdit" method="get">
							<input type="hidden" name="id" value="${user.id }"> <input
								type="submit" value="編集">
						</form>
					</td>
					<td><c:if test="${user.id != loginUser.id}">
							<form action="./IsWork" method="post">
								<input type="hidden" name="id" value="${user.id}">
								<c:if test="${user.is_work == 1}">
									<input type="submit" value="停止させる"
										onClick="return confirm('${user.name}さんのアカウントを停止させてもよろしいですか？')">
								</c:if>
								<input type="hidden" name="id" value="${user.id}">
								<c:if test="${user.is_work == 0}">
									<input type="submit" value="稼動させる"
										onClick="return confirm('${user.name}さんのアカウントを稼動させてもよろしいですか？')">
								</c:if>
						</form>
						</c:if></td>
				</tr>
			</c:forEach>
		</table>
		<br /> <a href="./">ホーム画面に戻る</a><br /> <br />
	</div>
</body>
<div class="copyright">Copyright(c)MikiHashiura</div>
</html>
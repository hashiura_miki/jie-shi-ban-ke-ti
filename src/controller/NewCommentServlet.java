package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		//HttpSession session = request.getSession();

		List<String> e_messages = new ArrayList<String>();
		e_messages.add("不正なアクセスです");
		//session.setAttribute("errorMessages", e_messages);
		request.setAttribute("errorMessages", e_messages);
		response.sendRedirect("login");

	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> comments = new ArrayList<String>();

		if (isTitleValid(request, comments)){

			User user = (User) session.getAttribute("loginUser");
			String messageId = request.getParameter("message_id");

			Comment comment = new Comment();
			comment.setText(request.getParameter("text"));
			comment.setUserId(user.getId());
			comment.setMessage_id(Integer.parseInt(messageId));
			new CommentService().register(comment);
			response.sendRedirect("./");
//改行含む500文字に適用未
		} else {
			session.setAttribute("errorMessages", comments);
			//request.setAttribute("errorMessages", comments);
			response.sendRedirect("./");
		}
	}

	private boolean isTitleValid(HttpServletRequest request, List<String> comments) {

		String comment = request.getParameter("text");

//		if (StringUtils.isEmpty(comment)) {
//			comments.add("コメントを入力してください");
//		}
		if (500 < comment.length()) {
			comments.add("コメントは500文字以下で入力して下さい");
		}
		if (comments.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
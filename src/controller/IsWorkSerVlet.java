package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;
/**
 * Servlet implementation class IsWorkSerVlet
 */
@WebServlet("/IsWork")
public class IsWorkSerVlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		int sql_id = Integer.parseInt(request.getParameter("id"));

		User user = new UserService().getUser(sql_id);

		request.setAttribute("user", user);

		new UserService().is_work(user);

		session.setAttribute("user", user);
		response.sendRedirect("./UserList");

	}

}
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/editStyle.css" rel="stylesheet" type="text/css">
<title>ユーザー編集</title>
</head>
<body>
<div class="main-contents">
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
		<form action="./UserEdit" method="post">
			<p class="txt">
				<br /> <label for="user_id">ユーザーID（半角英数 6～20文字）</label> <input
					required pattern="^[a-zA-Z0-9]+$" minlength='6' maxlength='20'
					name="user_id" id="user_id" value="${user_id}" /><br /> <br /> <label
					for="password">パスワード(半角英数記号 6～20文字)</label> <input
					pattern="^[a-zA-Z0-9!-/:-@¥[-`{-~]+$" minlength='6' maxlength='20'
					name="password" type="password" id="password" /> <br /> <label
					for="password2">パスワード再入力(確認用)</label> <input maxlength='20'
					name="password2" type="password" id="password2" /> <br /> <br />
				<label for="name">名前（10文字以内）</label> <input required maxlength='10'
					name="name" id="name" value="${name}" /> <br /> <br /> <select
					name="branch_id" value="${branch_id}">
					<c:if test="${branch_id == 1}">
						<option value="1" selected>本社</option>
					</c:if>
					<c:if test="${branch_id != 1}">
						<option value="1">本社</option>
					</c:if>
					<c:if test="${branch_id == 2}">
						<option value="2" selected>支店A</option>
					</c:if>
					<c:if test="${branch_id != 2}">
						<option value="2">支店A</option>
					</c:if>
					<c:if test="${branch_id == 3}">
						<option value="3" selected>支店B</option>
					</c:if>
					<c:if test="${branch_id != 3}">
						<option value="3">支店B</option>
					</c:if>
					<c:if test="${branch_id == 4}">
						<option value="4" selected>支店C</option>
					</c:if>
					<c:if test="${branch_id != 4}">
						<option value="4">支店C</option>
					</c:if>
				</select> <br /> <br /> <select name="position_id">
					<c:if test="${position_id == 1}">
						<option value="1" selected>総務人事</option>
					</c:if>
					<c:if test="${position_id != 1}">
						<option value="1">総務人事</option>
					</c:if>
					<c:if test="${position_id == 2}">
						<option value="2" selected>情報管理</option>
					</c:if>
					<c:if test="${position_id != 2}">
						<option value="2">情報管理</option>
					</c:if>
					<c:if test="${position_id == 3}">
						<option value="3" selected>支店長</option>
					</c:if>
					<c:if test="${position_id != 3}">
						<option value="3">支店長</option>
					</c:if>
					<c:if test="${position_id == 4}">
						<option value="4" selected>支店社員</option>
					</c:if>
					<c:if test="${position_id != 4}">
						<option value="4">支店社員</option>
					</c:if>

				</select> <br /> <br />
				<c:if test="${user.id != loginUser.id}">
				<c:if test="${is_work == 1}">
					<input type="radio" name="is_work" value="1" checked="checked">稼動<input
						type="radio" name="is_work" value="0">停止 </c:if>
				<c:if test="${is_work == 0}">
					<input type="radio" name="is_work" value="1">稼動<input
						type="radio" name="is_work" value="0" checked="checked">停止 </c:if></c:if>
				<br /> <br /> <input type="hidden" name="id" value="${user.id }">
				<input type="submit" value="登録"> <br /> <br /> <br /> <a
					href="./UserList">ユーザー管理画面に戻る</a> <br /> <br /> <a href="./">ホーム画面に戻る</a>
			</p>
		</form>
	</div>
	<div class="copyright">Copyright(c)MikiHashiura</div>
</body>
</html>